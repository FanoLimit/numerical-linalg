/*Learning to usde GSL primatives and flags.
Base algorithm is Golub and Van Loan, Matrix Computations
Algorith 4.2.1, GAXPY Varient, GSL Style conformant*/


#include <gsl/gsl_math.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_linalg.h>

/*This cholesky implementation tries to use GAXPY operations
and overwrites the original matrix's lower triangular part
with its Cholesky decomposition.  This is reflected across
the diag to get a totally symmetric decomp.*/


int gsl_cholesky_decomp(gsl_matrix* A){
    const size_t m=A->size1;
    const size_t n=A->size2;

    if (m!=n){
        GSL_ERROR("Cholesky requires a square matrix.",GSL_ENOTSQR);
    }
    else{
        size_t j;

        for (j=0;j<n;j++){
            double Ajj;

            gsl_vector_view v=gsl_matrix_subcolumn(A,j,j,n-j); /*A[j:n][j]*/

            if (j>0){
                gsl_vector_view w=gsl_matrix_subrow(A,j,0,j); /*(A[j][0:j-1])^T*/
                gsl_matrix_view x=gsl_matrix_submatrix(A,j,0,n-j,j); /*A[j:n][0:j]*/

                gsl_blas_dgemv(CblasNoTrans,-1.0,&x.matrix,&w.vector,1.0,&v.vector); /*v=v-wx*, Double GEneral Matrix Vector*/
            }

            Ajj=gsl_matrix_get(A,j,j); 

            if (Ajj<=0){
                GSL_ERROR("Matirx is not positive definite.",GSL_EDOM);
            }

            Ajj=sqrt(Ajj);
            gsl_vector_scale(&v.vector,1.0/Ajj);
        }
        gsl_matrix_transpose_tricpy('L',0,A,A);
        return GSL_SUCCESS;
    }
}

int main(){
    int b[]={18, 22,  54,  42,
                   22, 70,  86,  62,
                   54, 86, 174, 134,
                   42, 62, 134, 106};
    int dim=4;

    gsl_matrix* v=gsl_matrix_calloc(dim,dim);

    int i,j;
    for (i=0;i<dim;i++){
        for (j=0;j<dim;j++){
            gsl_matrix_set(v,i,j,b[i*dim+j]);
        }
    }

    gsl_cholesky_decomp(v);

    for (i=0;i<dim;i++){
        for (j=0;j<dim;j++){
            printf("%2.5f ",gsl_matrix_get(v,i,j));
        }
        printf("\n");
    }

    gsl_matrix_free(v);

    return 0;
}

