CC=gcc
CFLAGS=-Wall `gsl-config --cflags --libs`

TARGET=gsl_chole

all=$(TARGET)

$(TARGET):$(TARGET).c
	$(CC) -o $(TARGET) $(TARGET).c $(CFLAGS)

clean:
	$(RM) $(TARGET)
