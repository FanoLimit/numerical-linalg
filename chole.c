#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/* Base SAXPY Cholesky, takes a matrix A and its dim n.
 * Results are written to a new matrix.  No checks are run.
 */

double *cholesky(double *A,int n){
    double* chole=(double*) calloc(n*n,sizeof(double));

    if (chole==NULL){
        exit(EXIT_FAILURE);
    }

    for (int i=0;i<n;i++){
        for (int j=0;j<(i+1);j++){
            double s=0;
            for (int k=0;k<j;k++){
                s+=chole[i*n+k]*chole[j*n+k];
            }
            chole[i*n+j]= (i==j)?
                sqrt((A[i*n+i]-s)):
                1.0/chole[j*n+j]*(A[i*n+j]-s);
        }
    }
    return chole;
}

void print_matrix(double* A, int n){
    for (int i=0;i<n;i++){
        for (int j=0;j<n;j++){
            printf("%2.5f ",A[i*n+j]);
        }
        printf("\n");
    }
}

int main(){
    /*Used to verify results with a few samples
     */

    int n=3;
    double m1[]={25,15,-5,
                15,18,0,
                -5,0,11};

    double* c=cholesky(m1,n);

    print_matrix(c,n);
    printf("\n");

    free(c);

    double m2[]={18,22,54,42,
                22,70,86,62,
                54,86,174,134,
                42,62,134,106};

    n=4;
    double *c2=cholesky(m2,n);
    print_matrix(c2,n);
    printf("\n");
    free(c2);

    return 0;
}

